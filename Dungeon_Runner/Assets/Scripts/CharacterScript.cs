﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Actions
{
    string action;
    string animation;
}


public class CharacterScript : MonoBehaviour
{
    private CharacterController chc;
    private Vector3 playerVelocity;
    
    public float playerSpeed = 4.0f;
    public float playerRotateSpeed = 100.0f;
    public float jumpHeight = 1.0f;
    public float gravityValue = -9.81f;

    public Actions[] actions;

    private void Start()
    {
        chc = gameObject.GetComponent<CharacterController>();
    }

    void FixedUpdate()
    {
        
        if (chc.isGrounded && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        
            chc.transform.Rotate(new Vector3(0, Time.deltaTime* Input.GetAxis("Horizontal")* playerRotateSpeed, 0));
            chc.Move( transform.forward*Input.GetAxis("Vertical")* playerSpeed * Time.deltaTime );

        // Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        // chc.Move(move * Time.deltaTime * playerSpeed);

        // if (move != Vector3.zero)
        // {
        //     gameObject.transform.forward = move;
        // }

        // Changes the height position of the player..
        if (Input.GetButtonDown("Jump") && chc.isGrounded)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        chc.Move(playerVelocity * Time.deltaTime);
    }
}
